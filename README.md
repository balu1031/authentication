# authentication

 1. api for user registration
 http://localhost:1234/register

2.api for login:
http://localhost:1234/login

3.api to get salt for hashing:
http://localhost:1234/getsalt

4. api to get existing user profile:
http://localhost:1234/userprofile

5. api for adding todo activity:
http://localhost:1234/addtodo

6.api for getting todos:
http://localhost:1234/gettodos
